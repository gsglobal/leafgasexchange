Leaf gas exchange database
========================================

This database accompanies the publication "**Lin et al. (2015) Optimal stomatal behaviour around the world. Nature Climate Change 5, 459�464 (2015)**"
DOI: 10.1038/NCLIMATE2550 

**Update:**
A single csv file which includes all the data sets used in the manuscript can now be downloaded via figshare:

http://figshare.com/articles/Optimal_stomatal_behaviour_around_the_world/1304289

Update 20-11-2015: The database available here has been expanded to include all variables given to us by data authors. 
The concise version, with only variables used in Lin et al. (2015), is still available through Figshare.

**Update 27-09-2017**: All leaf-level data used in the publication **Knauer et al. (2017) "Towards physiologically meaningful water-use efficiency estimates
from eddy covariance data". Global Change Biology, DOI: 10.1111/gcb.13893** can now be found in this repository.

**Contact**

*  Yan-Shih Lin (yanshihL [at] gmail.com)
*  Belinda Medlyn (b.medlyn [at] westernsydney.edu.au)
*  Remko Duursma (remkoduursma [at] gmail.com)
*  J�rgen Knauer (jknauer [at] bgc-jena.mpg.de)


Variable | Description | Units or allowed values
---------|-------------|------
Pathway | Photosynthetic pathway | C3, C4, CAM
Type  | Gymnosperm or angiosperm | 'gymnosperm', 'angiosperm'
Plantform   | Plant life form | 'tree', 'shrub', 'grass', 'crop', 'savanna' 
Leafspan  | Evergreen or Deciduous | 'evergreen', 'deciduous' 
Tregion  | Biome | 'arctic', 'boreal', 'temperate', 'tropical'
Wregion  | Classification based on aridity index | 'arid', 'semi-arid', 'dry sub-humid', 'humid' 
Wregion2  | Moisture index | (numerical)  
opt  | Growing under ambient or stressed condition | 'opt' or 'non-opt' 
fitgroup  | data grouping tag (optional) | (string)
Date  | Date of the year when measurement conducted |  
Time |Time of the day when measurement conducted |  
decimaltime  | 0 - 24 | Hours
Datacontrib  | Name of the data contributor | (string)
Species  | Species name | (string)
Funtype  |  Plant functional type (optional | (string) 
Funtype2v | Plant functional type, second definition| (string)
Location  | Site location | (string) 
VPD  | Vapour pressure deficit | kPa 
VPD_A  | Vapour pressure deficit gap filling based on air T | kPa
VPD_L  | Vapour pressure deficit gap filling based on leaf T| kPa
RH  | relative humidity | %
Tair  |  Air temperature | degrees C 
Tleaf  | Leaf temperature | degrees C 
CO2S  | CO2 concentration at leaf surface | ppm
PARin  | Photosynthetically active radiation in cuvette | umol m-2 s-1
Patm  | Atomospheric pressure | kPa
Photo  | Photosynthetic rate | umol m-2 s-1
Cond  | Stomatal conductance | mol m-2 s-1
BLCond  | Boundary layer conductance| mol m-2 s-1
Ci  | Intercelllular [CO2] | ppm 
Trmmol  | Leaf transpiration | mmol m-2 s-1 
Rdark  | Dark respiration rate | umol m-2 s-1 
PARout  | Photosynthetically active radiation in cuvette | umol m-2 s-1
SWC  | Soil water content | m3 m-3
SWP  | Soil water potential | MPa
latitude  | Site latitude | Decimal degrees
longitude  | Site longitude | Decimal degrees
altitude  | Site altitude | m
Totalheight  | Total plant height | m 
LAI  | Leaf area index | m2 m-2
sampleheight  | Height of measurement | m 
canopyposition  | | 'Sunlit' or 'shaded' leaf, 'top' or 'lower' canopy  
nperc  | Leaf nitrogen concentration | % (mass) 
SLA  | Specific leaf area | cm2 g-1
leafage  | Leaf age | years 
leafsize  | Leaf size | cm2 
leaflen  | Leaf length | cm 
LWP  | Leaf water potential | MPa 
LWPpredawn  | Pre-dawn leaf water potential | MPa 
Instrument  | Instrument used for measurement | e.g. Licor 6400, automated cuvette, etc
Season  | Season when the measurement conducted | (string)
GrowthCa  | Growth [CO2] | 'ambient' or ppm 
GrowthTair  | Growth Tair | 'ambient' or degree C 
Growthcond  | Growth condition | 'Glasshouse', ' Field', 'Potted plant', 'Whole-tree chamber', etc. 
Treatment  | Experimental treatment | 'fertilization', 'irrigation', etc. 
TreeNum  | Tree number | (string) 
LeafNum  | Leaf number | (string) 
OriginalFile  | Original file name from data contributor | (string)
Comments  | Anything useful related to the data | (string)
Reference  | Referee associated with the data set | (string)
LightSource  | Light source used in the cuvette| (string)